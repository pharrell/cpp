/** \file   arrfun1main.cpp
    \brief  Use function to manipulate array
*/

#include <iostream>
const int ArSize = 8;
int sum_arr(const int arr[], int n);  // function prototype

/*! \brief main func in which the ``sum_arr`` will be called.
 *
 */
int main()
{
  using namespace std;
  int cookies[ArSize] = {1, 2, 4, 8, 16, 32, 64, 128};
  /// Some systems require preceding int with static to enable array initialization.
  int sum = sum_arr(cookies, ArSize);
  cout << "Total cookies eaten: " << sum << "\n";
  return 0;
}


/*! \brief Summation of array elements
 *
 *  Return the sum of an integer array
 */
 int sum_arr(const int * const arr, int n) // a constant pointer to constant int value
{
   int total = 0;
   for (int i=0; i<n; i++)
     total += arr[i];
   return total;
}