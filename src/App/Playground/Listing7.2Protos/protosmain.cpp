/** \file    protosmain.cpp
    \brief   Using prototypes and function calls
*/

#include <iostream>
void cheers(int);         // function prototype
double cube(double x);    // function prototype

/*! \brief The usage of function call.
 *
 */
int main()
{
  ///
  /// This is just a very simple example.
  ///
  using namespace std;
  cheers(5);                             // function call
  cout << "Give me a number: ";
  double side;
  cin >> side;
  double volume = cube(side);            // function call
  cout << "A " << side << "-foot cube has a volume of ";
  cout << volume << " cubic feet.\n";
  cheers(static_cast<int>(cube(2)));     // prototype protection at work
  return 0;
}

/*! \brief Prints "Cheers!".
 *
 *  Prints "Cheers!" multiple times according to the value of the int argument.
 */
void cheers(int n)
{
  using namespace std;
  for (int i=0; i<n; i++)
    cout << "Cheers! ";
  cout << endl;
}

/*! \brief Calculates cubic value.
 *
 *  Takes a double value, then calculates the cubic value.
 */
double cube(double x)
{
  return x * x * x;
}
