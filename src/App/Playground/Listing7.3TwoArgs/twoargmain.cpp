/** \file    twoargmain.cpp
    \brief   A function with 2 arguments
*/

#include <iostream>

using namespace std;

void n_chars(char, int);

/*! \brief Yet another simple example.
 *
 * @return 0
 */
int main()
{
  int times = 0;
  char ch;

  cout << "Enter a character: ";
  cin >> ch;
  while (ch!='q')       // q to quit
  {
    cout << "Enter an integer: ";
    cin >> times;
    n_chars(ch, times); // function with two arguments
    cout << "\nEnter another character or press the q-key to quit: ";
    cin >> ch;
  }
  if (ch == 'q') {
    cout << "You have chosen to quit.\n";
    cout << "The value of times is " << times << ".\n";
  }
  cout << "Bye.\n";
  return 0;
}

/*! \brief display a character n times.
 *
 *
 */
void n_chars(char c, int n)  // displays c n times.
{
  while (n-- > 0)       // continue until n reaches 0
  {
    cout << c;
  }
}