/** \file arrfun3main.cpp
    \brief More array operations, 1. fill array, 2. show array, 3. revalue
*/
#include <iostream>
const int Max = 5;

// function prototypes
int fill_array(double ar[], int limit);
void show_array(const double ar[], int n);  // don't change data
void revalue(double r, double ar[], int n);

int main()
{
  using namespace std;
  double properties[Max];

  int size = fill_array(properties, Max);
  show_array(properties, size);

  if (size > 0)
  {
    cout << "Enter revaluation factor: ";
    double factor;
    while (!(cin >> factor)) // bad input
    {
      cin.clear();
      while(cin.get()!='\n')
        continue;
      cout << "Bad input; Please enter a number: ";
    }
    revalue(factor, properties, size);
    show_array(properties, size);
  }
  cout << "Done.\n";
  cin.get();
  cin.get();
  return 0;
}

int fill_array(double ar[], int limit)
{
  using namespace std;
  double tmp;
  int i;
  for (i = 0; i < limit; i++)
  {
    cout << "Enter value #" << (i+1) << ": ";
    cin >> tmp;
    /// ``!cin`` would be evaluated to **true** if
    /// the input value is not number.
    if (!cin){  // bad input, or unexpected input.
      cin.clear();  // cin.clear() will clear the 'bad' status.
      while(cin.get()!='\n')
        continue;
      cout << "Bad input; input process terminated.\n";
      break;
    } else if (tmp < 0){
      break;
    }
    ar[i] = tmp;
  }
  return i;
}

// the following function can use, but not alter,
// the array whose address is ar
void show_array(const double ar[], int n)
{
  using namespace std;
  for (int i = 0; i<n; i++)
  {
    cout << "Property #" << (i+1) << ": $";
    cout << ar[i] << endl;
  }
}

//multiplies each element of ar[] by r
void revalue(double r, double ar[], int n)
{
  for (int i = 0; i < n; i++)
    ar[i] *= r;
}