/** \file BaseConvMain.cpp
    \brief Base conversion.
    1. decimal to any (binary, octal, hex)
    2. binary to decimal
    3. octal to binary
 */

#include "iostream"
#include "../../../Lib/LibBattleground/Decimal2Any.h"
#include <list>

int main()
{
//  std::cout << "\n########################################" << std::endl;
//  std::cout << "# Path: App/Battleground/BaseConversion " << std::endl;
//  std::cout << "# Func: Base Conversion  " << std::endl;
//  std::cout << "########################################" << std::endl;

  Decimal2Any De2B(226, BINARY);
  Decimal2Any De2O(226, OCTAL);
  Decimal2Any De2H(226, HEXADECIMAL);
  De2B.Convert();
  De2O.Convert();
  De2H.Convert();

  std::cout << 3 / 4 << std::endl;
  std::cout << 7 / 4 << std::endl;

  std::list<int> ListInt;
  std::cout << "============" << std::endl;
  std::cout << ListInt.empty() << std::endl;
  std::cout << ListInt.size() << std::endl;
  ListInt.push_back(3);
  std::cout << ListInt.size() << std::endl;
  std::cout << "===========================0000000000000000000000" << std::endl;
  std::cout << *ListInt.begin() << std::endl;
  std::cout << *ListInt.end() << std::endl;
  std::cout << "================0000000000000000000000000000000000000" << std::endl;
  for (std::list<int>::iterator it=ListInt.begin();it!=ListInt.end();it++)
    std::cout << *it << std::endl;
}