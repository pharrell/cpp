//
// Created by WANG Zhenxiang Pharrell on 20/8/2018.
//

#ifndef CPP_DECIMAL2ANY_H
#define CPP_DECIMAL2ANY_H

#include "TypeDef.h"

class Decimal2Any {
private:
  ///< by default access to members of a class is private.
  int m_iDecimalNum;
  DesBase m_eDesBase;

public:
  Decimal2Any(int a, DesBase db);

  ~Decimal2Any() = default;

  void Convert();
};


#endif //CPP_DECIMAL2ANY_H
