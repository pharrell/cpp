/** \file    tappshiftmain.cpp
    \brief   Demonstrates the effects of shift operators
*/

#include <iostream>
#include <stdio.h>
#include <math.h>

//! \ingroup TAppShift
//! \{
int main() {
  for (int i = 0; i < 3; i++) {
    std::cout << (64 >> (i + 1)) << std::endl;
  }
  std::cout << "============= unit: bytes" << std::endl;
  std::cout << "int                :" << sizeof(int) << std::endl;
  std::cout << "unsigned int       :" << sizeof(unsigned int) << std::endl;
  std::cout << "short int          :" << sizeof(short int) << std::endl;
  std::cout << "long int           :" << sizeof(long int) << std::endl;
  std::cout << "long long int      :" << sizeof(long long int) << std::endl;
  std::cout << "char               :" << sizeof(char) << std::endl;
  std::cout << "unsigned char      :" << sizeof(unsigned char) << std::endl;
  std::cout << "bool               :" << sizeof(bool) << std::endl;
  std::cout << "float              :" << sizeof(float) << std::endl;
  std::cout << "double             :" << sizeof(double) << std::endl;
  std::cout << "wchar_t            :" << sizeof(wchar_t) << std::endl;
  std::cout << "long double        :" << sizeof(long double) << std::endl;

  int x[3];
  std::cout << "~~~~~~~~~~~~~" << std::endl;
  std::cout << sizeof(x) << std::endl;
  std::cout << sizeof(x[0]) << std::endl;
  return 0;
}
//! \}