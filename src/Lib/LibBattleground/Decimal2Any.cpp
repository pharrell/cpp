//
// Created by WANG Zhenxiang Pharrell on 20/8/2018.
//

#include "Decimal2Any.h"
#include <vector>
#include "iostream"

Decimal2Any::Decimal2Any(int a, DesBase b) {
  m_iDecimalNum = a;
  m_eDesBase = b;
}

void Decimal2Any::Convert() {
  std::vector<int> vRes;
  int base = m_eDesBase;
  int num = m_iDecimalNum;

  // Conversion starts
  while (num != 0)
  {
    int remainder = num % m_eDesBase;
    vRes.push_back(remainder);
    num /= base;
  }

  fprintf( stdout, "\n-----------------------------------------\n" );
  fprintf( stdout, "Base [10] to [%d]", m_eDesBase );
  fprintf( stdout, "\n" );
  fprintf( stdout, "%d ---> ", m_iDecimalNum);
  for (auto ir = vRes.rbegin(); ir != vRes.rend(); ++ir)
  {
    if (*ir == 10) {
      std::cout << "A";
    } else if (*ir == 11) {
      std::cout << "B";
    } else if (*ir == 12) {
      std::cout << "C";
    } else if (*ir == 13) {
      std::cout << "D";
    } else if (*ir == 14) {
      std::cout << "E";
    } else if (*ir == 15) {
      std::cout << "F";
    } else {
      std::cout << *ir;
    }
  }
  fprintf( stdout, "\n-----------------------------------------\n" );
  std::cout << "\n";
}