//
// Created by WANG Zhenxiang Pharrell on 20/8/2018.
//
/**
 * \file TypeDef.h
 * \brief Define enumerations
 */

#ifndef CPP_TYPEDEF_H
#define CPP_TYPEDEF_H

enum DesBase ///> Destination Base, can be the value form 2, 8, 16
{
  BINARY      =  2,
  OCTAL       =  8,
  HEXADECIMAL = 16,
  NUM_OF_DESTINATION_BASES = 3,
};

#endif //CPP_TYPEDEF_H