// read a file into memory
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

int main () {
  std::ifstream is ("test.txt", std::ifstream::binary);
  if (is) {
    // get length of file:
    std::cout << "is.end: " << is.end << std::endl;
    std::cout << "is.beg: " << is.beg << std::endl;
    std::cout << "is.tellg() before seekg to end: " << is.tellg() << std::endl;
    // In this example, seekg is used to move the position to the end of the file, and then back to the beginning.


//    // position to the nth byte of fileObject (assumes ios::beg)
//    fileObject.seekg( n );
//
//    // position n bytes forward in fileObject
//    fileObject.seekg( n, ios::cur );
//
//    // position n bytes back from end of fileObject
//    fileObject.seekg( n, ios::end );
//
//    // position at end of fileObject
//    fileObject.seekg( 0, ios::end );
    is.seekg(3);
    std::cout << "\nis.cur: " << is.cur << std::endl;
    std::cout << "\nis.tellg(): " << is.tellg() << std::endl;

    is.seekg (0, is.end);  // seekg(): Sets the position of the next character to be extracted from the input stream.
    int length = static_cast<int>(is.tellg());  // tellg(): Returns the position of the current character in the input stream
    std::cout << "is.tellg() after seekg to end: " << is.tellg() << std::endl;
    std::cout << "length: " << length << std::endl;
    is.seekg (0, is.beg);

    // allocate memory:
    char * buffer = new char [length];

    // read data as a block:
    is.read (buffer,length);

    is.close();

    // print content:
    std::cout.write (buffer,length);

    delete[] buffer;
  } else {
    std::cerr << "test.txt not found." << std::endl;
  }

  return 0;
}