/** \file    probmain.cpp
    \brief   Probability of winning
*/

#include <iostream>

long double probability(unsigned numbers, unsigned picks);

/*! \brief main func in which probability func is called.
 *
 * @return 0
 */
int main()
{
  using namespace std;
  unsigned total, choices;
  cout << "Enter the total number of choices on the game card and\n"
          "the number of picks allowed:\n";
  while ((cin>>total>>choices) && choices <= total )
  {
    cout << "You have one chance in ";
    cout << probability(total, choices);
    cout << " of winning. \n";
    cout << "Next two numbers (q to quit): ";
  }
  cout << "bye\n";
  return 0;
}

/*! \brief Probability calculation
 *
 *  The following function calculates the
 *  probability of picking picks numbers
 *  correctly from numbers choices.
 *
 */
 long double probability(unsigned numbers, unsigned picks)
{
   ///
   /// There are two kinds of local vars that you can have in
   /// a function: first there are formal parameters, second
   /// come the other local vars.
   ///
   long double result = 1.0; // here come some local variables
   long double n;
   unsigned p;  // equivalent to ``unsigned int``

   for (n=numbers, p=picks; p>0; n--, p--)
     result = result * n / p;
   return result;
}