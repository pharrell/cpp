cmake_minimum_required(VERSION 3.12)
project(Listing7.7arrfun3)

set(CMAKE_CXX_STANDARD 17)

set(SOURCE_FILES arrfun3main.cpp)
add_executable(Listing7.7arrfun3 ${SOURCE_FILES})